# welcome to git bitBucket
#loading the dataset of 20,000 observations loc at url
source("http://www.openintro.org/stat/data/cdc.R")
# retrieving variable names
names(cdc)
# from cmd function above, the console index [1] showed  variables and
#Each one of these variables corresponds to a question that was asked in the survey
#For example, for genhlth, respondents were asked to evaluate their general health, responding
#either excellent, very good, good, fair or poor. 

tail(cdc)
# Answer Exercise 1,from the console, index 1 shows 9 variables with 20000 observations.confirmed by global environment
# only 'genhlth' and 'gender' var are of categorical data type the rest of the variables are of discrete datatype.
summary(cdc$weight)
# using r as calculator
190.0-140.0
# summary statistics
mean(cdc$weight)
# variance weight
var(cdc$weight)
# standard deviation weight
sqrt(var(cdc$weight))
median(cdc$weight)
# categorical data analysis
table(cdc$smoke100)
#relative freq distribution
table(cdc$smoke100)/20000
# nested barplot function with output: about 9500 smoke more than 100cigtts and remainder 10500 smoke less
barplot(table(cdc$smoke100))
# alternatively
smoke<-table(cdc$smoke100)
barplot(smoke)
#Exercise2 numerical summary for height and age
summary(cdc$height)
summary(cdc$age)
#InterQuartile Range IQR of height :an object called alocate is assigned to summary height function
alocate<-summary(cdc$height)
IQR(alocate)
# IQR of variable age assigning object called zvikaramba
zvikaramba<-summary(cdc$age)
IQR(zvikaramba)
# relative freq distribution of exerany and gender
table(cdc$exerany)/20000
table(cdc$gender)/20000
# number of males in the sample
table(cdc$gender,exclude = "f")
# proportion in excellent health:totalgenhlth minus sum of all excluding excellent
table(cdc$genhlth)
sum(table(cdc$genhlth,exclude = "excellent"))
sum(table(cdc$genhlth))-sum(table(cdc$genhlth,exclude = "excellent"))
# alternative code giving same result , returns excellent TRUE
table(cdc$genhlth=="excellent")
# data frames
dim(cdc)
# retrieving weight data about 567th observation
cdc[567,6]
#To see the weights for the first 10 respondents 
cdc[1:10,6]
#create a new dataset called mdata  and the data be just for men. 
mdata <- subset(cdc, cdc$gender == "m")
# several rows object mdata
head(mdata)
# retrieving data for men over 30 years
menOver30 <- subset(cdc,cdc$gender=="m"& cdc$age>30)
#use of pipe meaning or
menOver30 <- subset(cdc, cdc$gender == "m" | cdc$age > 30)
#boxplot to provide a thumbnail sketch of a variable height
# Exercise 3.Confirmating that meadian and upper and lower
#quartiles match those given by summary height function
boxplot(cdc$height)
#boxplot of height being function of gender
boxplot(cdc$height~cdc$gender)
#make a new object called bmi and then creates boxplots of these
#values, defining groups by the variable cdc$genhlth.
#bmi <- (cdc$weight / cdc$height~2) * 703 
# making objects a and b as numerator and denominator for bmi
a<- 703*cdc$weight
b<-cdc$height*cdc$height
bmi<-a/b
boxplot(bmi~cdc$genhlth)
#Exercise 4 the boxplot shows a steady increase in bmi over decrease in genhealth
# below :men have a higher bmi than women naturally weight of men is greater than women's
boxplot(bmi~cdc$gender)
#below bmi tends to be lower in early and late adulthood.
boxplot(bmi~cdc$age)
# below:there are more people in the middle age than ealy and late adulthood
hist(cdc$age)
# below:histogram bmi ,freq almost normally distributed
hist(bmi)
# shows a right skewed distribution
hist(bmi,breaks = 50)
